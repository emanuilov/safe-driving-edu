﻿using UnityEngine;
using System.Collections.Generic;
using Leap;
using Leap.Unity;

public class AttachSteeringWheel : MonoBehaviour
{
    LeapProvider provider;
    bool wheelCollision;

    void Start()
    {
        provider = FindObjectOfType<LeapProvider>() as LeapProvider;
    }

    void Update()
    {
        Frame frame = provider.CurrentFrame;
        List<Hand> hands = frame.Hands;
        float angle;

        if (frame.Hands.Count == 0)
        {
            return;
        }

        if (frame.Hands.Count == 2)
        {
            var angleLeftHand = getAngle(transform.position, hands[0].PalmPosition.ToVector3());
            var angleRightHand = getAngle(transform.position, hands[1].PalmPosition.ToVector3());
            angle = 180 - (angleLeftHand + angleRightHand) / 2;
        }
        else
        {
            angle = getAngle(transform.position, hands[0].PalmPosition.ToVector3());

            if (hands[0].IsRight)
            {
                angle = 150 - angle;
            }
            else
            {
                angle = 230 - angle;
            }
        }

        foreach (Hand hand in frame.Hands)
        {
            if (hand.GrabStrength == 1 && wheelCollision == true)
            {
                transform.rotation = Quaternion.Euler(angle, 0, 0);
            }
        }
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        wheelCollision = true;
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        wheelCollision = false;
    }

    private float getAngle(Vector3 vectorA, Vector3 vectorB)
    {
        Vector3 diference = vectorB - vectorA;
        float sign = (vectorB.y < vectorA.y) ? 1.0f : -1.0f;
        return Vector3.Angle(Vector3.back, diference) * sign;
    }
}