﻿using UnityEngine;
using System.Collections;

public class EndGameCollision : MonoBehaviour {

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.name == "trigger") {
            Application.LoadLevel("EndGame");
        }
    }
}
